package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  // test("find and update") {
  //   assert(findUpdate('a', Nil) === List(('a', 1)))
  //   assert(findUpdate('a', List(('a', 1))) === List(('a', 2)))
  //   assert(findUpdate('a', List(('o', 3))) === List(('a', 1), ('o', 3)))
  // }

  // test("timesAcc") {
  //   assert(timesAcc(Nil, List(('a', 1))) === List(('a', 1)))
  //   assert(timesAcc(List('a', 'o'), List(('a', 1))) === List(('a', 2), ('o', 1)))
  //   assert(timesAcc(List('a', 'o'), Nil) === List(('a', 1), ('o', 1)))
  // }

  test("times test") {
    assert(times(List('a', 'b', 'a')) === List(('a', 2), ('b', 1)))
  }

  // test("makeAcc") {
  //   assert(makeAcc(List(('t', 2), ('e', 1), ('x', 3)), Nil) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  //   assert(makeAcc(List(('t', 2), ('e', 1), ('x', 3)), List(Leaf('y',4))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3), Leaf('y',4)))
  // }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List()) === List())
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("singleton") {
    assert(singleton(Nil) === false)
    assert(singleton(List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3))) === true)
    assert(singleton(List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4),
      Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4))) === false)
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("until") {
    val treeUno = Fork(Leaf('a', 3), Leaf('q', 10), List('a', 'q'), 13)
    val treeDos = Fork(Leaf('e', 1), Leaf('i', 2), List('e', 'i'), 3)
    assert(until(singleton, combine)(List(treeUno, treeDos)) === List(Fork(treeUno, treeDos, List('a', 'q', 'e', 'i'), 16)))
  }

  test("createCodeTree") {
    assert(createCodeTree("abb".toList) === Fork(Leaf('a', 1), Leaf('b', 2), List('a', 'b'), 3))
  }

  // test("decoded secret") {
  //   assert(decodedSecret === " ")
  // }

  test("inList") {
    assert(inList(List('a', 'o'), 'a'))
    assert(!inList(List('a', 'o'), 'e'))
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("decode and quickEncode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
    }
  }


  trait TestCodeTables {
    val tableOne = List(('a', List(1)))
    val tableTwo = List(('o', List(0)))
    val emptyTable = List()
    val tableThree = List(('a', List(1)), ('e', List(0, 0)))
    val tableFour = List(('o', List(0, 1)), ('e', List(1, 1, 1)))
  }

  test("addBits") {
    assert(addBits(('a', List(1, 1)), ('a', List(0))) === ('a', List(1, 1, 0)))
  }

  test("convert") {
    assert(convert(Fork(Fork(Leaf('a', 1), Leaf('b', 2), List('a', 'b'), 3),Leaf('d', 3), List('a', 'b', 'd'), 6)) === List(('a',List(0, 0)), ('b',List(0, 1)), ('d',List(1))))
  }

  // test("updateCodeList") {
  //   assert(updateCodeList(('a', List(1, 1)), List()) === List(('a', List(1, 1))))
  //   assert(updateCodeList(('a', List(1, 1)), List(('a', List(0)), ('b', List(1)))) ===
  //     List(('a', List(1, 1, 0)), ('b', List(1))))
  //   assert(updateCodeList(('a', List(1, 1)), List(('i', List(0)), ('a', List(0)), ('b', List(1)))) ===
  //     List(('i', List(0)), ('a', List(1, 1, 0)), ('b', List(1))))
  // }

  // test("mergeCodeTables simple") {
  //   new TestCodeTables {
  //     assert(mergeCodeTables(emptyTable, emptyTable) === List())
  //     assert(mergeCodeTables(tableOne, emptyTable) === List(('a', List(1))),
  //       "with emptyTable")
  //     assert(mergeCodeTables(emptyTable, tableOne) === List(('a', List(1))),
  //       "with emptyTable")
  //     assert(mergeCodeTables(tableOne, tableTwo) === List(('a', List(1)), ('o', List(0))),
  //       "one element lists")
  //     assert(mergeCodeTables(tableOne, tableThree) === List(('a', List(1, 1)), ('e', List(0, 0))),
  //       "one element list with bigger list")
  //     assert(mergeCodeTables(tableTwo, tableThree) === List(('a', List(1, 1)), ('e', List(0, 0)), ('o', List(0))),
  //       "one element lists with bigger list 2")
  //     assert(mergeCodeTables(tableThree, tableFour) === List(('a', List(1)),
  //      ('o', List(0, 1)),  ('e', List(0, 0, 1, 1, 1))),
  //       "big tables")
  //   }
  // }

}
